﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Knight : Character
    {
        public Knight(string name, GenderEnum gender, RaceEnum race) : base(name, gender, race, ClassEnum.Knight, 22, 8, 10)
        {

        }

        public Knight(string name, GenderEnum gender, RaceEnum race, ClassEnum className) : base(name, gender, race, className, 22, 8, 10)
        {

        }

        public override void SpecialAttack()
        {
            Console.WriteLine($"{Race} uses all his might to strike his foe for {Strength * 10} damage!");
        }

        public override string ToString()
        {
            return base.ToString("Knight");
        }

        public virtual string ToString(string className)
        {
            return base.ToString(className);
        }

        public virtual string GetClassName()
        {
            return ClassEnum.Knight.ToString();
        }
    }
}
