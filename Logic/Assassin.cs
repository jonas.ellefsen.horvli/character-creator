﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Assassin : Character
    {
        public Assassin(string name, GenderEnum gender, RaceEnum race) : base(name, gender, race, ClassEnum.Assassin, 12, 12, 16)
        {

        }
        public Assassin(string name, GenderEnum gender, RaceEnum race, ClassEnum className) : base(name, gender, race, className, 12, 12, 16)
        {

        }
        public override void SpecialAttack()
        {
            Random random = new Random();
            int numberOfAttacks = random.Next(2, Intelligence / 2);
            Console.WriteLine($"{Race} makes a flurry of attacks!");
            for (int i = 0; i < numberOfAttacks; i++)
            {
                Console.WriteLine($"{Race} attacks for {Strength + Agility * random.Next(1, 3)} damage!");
            }
        }

        public override string ToString()
        {
            return base.ToString("Assassin");
        }
    }
}
