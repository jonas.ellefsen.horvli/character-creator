﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Archer: Character
    {
        public Archer(string name, GenderEnum gender, RaceEnum race): base(name, gender, race, ClassEnum.Archer, 10, 10, 15) { }

        public override void SpecialAttack()
        {
            Console.WriteLine($"{Name} shoots a volley of arrows and deals {Agility * Strength} damage!");
        }

        public override string ToString()
        {
            return base.ToString("Archer");
        }
    }
}
