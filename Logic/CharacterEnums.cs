﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public enum GenderEnum
    {
        Male = 1,
        Female
    }
    public enum ClassEnum
    {
        Knight = 1,
        BattleMage,
        Assassin,
        Defender,
        Archer,
        Healer
    }
    public enum RaceEnum
    {
        Human = 1,
        Dwarf,
        Elf,
        Orc
    }
}
