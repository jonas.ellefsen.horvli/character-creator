﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Defender: Knight
    {
        public Defender(string name, GenderEnum gender, RaceEnum race) : base(name, gender, race, ClassEnum.Defender) { }

        public override string ToString()
        {
            return base.ToString("Defender");
        }

        public override void SpecialAttack()
        {
            Console.WriteLine($"{Race} uses all his might to strike his foe for {Strength * 8} damage!");
        }

        public void Defend(Character target)
        {
            Console.WriteLine($"{Race} defends {target.Race}!");
        }

        public override string GetClassName()
        {
            return base.GetClassName() + " " + ClassEnum.Defender.ToString();
        }
    }
}
