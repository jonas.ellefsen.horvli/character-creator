﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Healer: BattleMage
    {
        public Healer(string name, GenderEnum gender, RaceEnum race) : base(name, gender, race, ClassEnum.Healer) { }

        public void Heal(Character target)
        {
            Console.WriteLine($"{Race} heals {target} for {Intelligence * 50}hp!");
        }

        public override string ToString()
        {
            return base.ToString("Healer");
        }
    }
}
