﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Logic
{
    public class DataHandler
    {
        public static void WriteFile(string text)
        {
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            File.WriteAllText(Path.Combine(docPath, $"Character.txt"), text);
        }

        public static void AppendCharacter(object character)
        {
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Character.txt";
            Console.WriteLine(docPath);
            string jsonData;
            List<object> characterList;
            if (System.IO.File.Exists(docPath))
            {
                jsonData = System.IO.File.ReadAllText(docPath);
                characterList = JsonSerializer.Deserialize<List<object>>(jsonData);
            } else
            {
                characterList = new List<object>();
            }
            characterList.Add(character);
            WriteFile(JsonStringify(characterList));
        }

        public static string JsonStringify(List<object> characterList)
        {
            var options = new JsonSerializerOptions
            {
                WriteIndented = true,
            };
            return JsonSerializer.Serialize(characterList, options);
        }
    }
}
