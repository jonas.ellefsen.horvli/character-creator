using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public abstract class Character
    {
        public string Name { get; protected set; }
        public GenderEnum Gender { get; protected set; }
        public RaceEnum Race { get; protected set;  }
        public ClassEnum ClassName { get; protected set; }
        public int Strength { get; protected set; }
        public int Intelligence { get; protected set; }
        public int Agility { get; protected set; }
        public int HealthPoints { get; protected set; }
        public int Energy { get; protected set; }
        public int ArmorRating { get; protected set; }

        protected Character(string name, GenderEnum gender, RaceEnum race, ClassEnum className, int strength, int intelligence, int agility)
        {
            Name = name;
            Gender = gender;
            Race = race;
            ClassName = className;
            Strength = 5 + strength;
            Intelligence = 5 + intelligence;
            Agility = 5 + agility;
            HealthPoints = ((strength * 2) + intelligence + agility) * 100;
            Energy = ((strength / 2) + intelligence + agility) * 100;
            ArmorRating = (strength^2 / agility) * 100;
        }

        public void Move()
        {
            Console.WriteLine($"{Race} moved.");
        }
        public void Attack()
        {
            int attackDamage = (Strength * 2 + Agility) / 2;
            Random random = new Random();
            int hitChance = random.Next(1, (Intelligence * 2 + Agility));
            if (hitChance < 10)
            {
                Console.WriteLine($"{Race} tried to attack and missed!");
            }
            Console.WriteLine($"{Race} attacked and dealt {attackDamage} damage!");
        }

        public abstract void SpecialAttack();

        protected string StatsToString()
        {
            return $"Strength: {Strength}\nIntelligence: {Intelligence}\nAgility: {Agility}\nHealthPoints: {HealthPoints}\nEnergy: {Energy}\nArmorRating: {ArmorRating}";
        }

        public virtual string ToString(string className)
        {
            return $"Name: {Name}\nGender: {Gender}\nRace: {Race}\nClass: {className}\n{StatsToString()}";
        }
    }
}
