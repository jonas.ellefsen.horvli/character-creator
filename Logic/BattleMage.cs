﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class BattleMage : Character
    {
        public BattleMage(string name, GenderEnum gender, RaceEnum race) : base(name, gender, race, ClassEnum.BattleMage, 5, 25, 10) { }

        public BattleMage(string name, GenderEnum gender, RaceEnum race, ClassEnum className) : base(name, gender, race, className, 5, 25, 10)
        {

        }

        public override void SpecialAttack()
        {
            Random random = new Random();
            Console.WriteLine($"{Race} conjures a spell and hits the enemy for { Intelligence * random.Next(5, 20)} damage!");
        }

        public override string ToString()
        {
            return base.ToString("Mage");
        }

        public override string ToString(string className)
        {
            return base.ToString(className);
        }
    }
}
