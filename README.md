#CharacterCreator

A .Net Winform project where you can create rpg characters and automatically store them in a sql database.

Created by Jonas.
External logic dll provided by Cem Pedersen.

## Components

### Frontend

#### Form1
Contains all the display logic for buttons and input components.

#### SqlHelper
Class containing static functions for storing RPG characters in database.

### Logic

#### Character
Class with the simplest form of an RPG character.

#### Knight
Subtype of Character and a strength focused RPG Character

##### Defender
Subtype of Knight with the same stats but different abilities.

#### BattleMage
Subtype of Character and a intelligence focused RPG Character

##### Healer
Subtype of BattleMage with the same stats but different abilities.

#### Assassin
Subtype of Character and a agility focused RPG character

##### Archer
Subtype of Assassin with the same stats but different abilities.

#### DataHandler
Class that contains functions for handling all character serialization made to text file.
Text file is stored under user/documents/characters.txt as json objects.

#### CharacterEnums
Contains all the enums used for types of Gender, Race and Class.

### RPGCharacterGeneratorLogic
dll containing character another somewhat similar character logic.
Provided by Cem Pedersen.
