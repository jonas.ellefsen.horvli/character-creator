﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Data.SqlClient;
using RPGCharacterGeneratorLogic;
using System.Data;

namespace FrontendBasic
{
    public class SqlHelper
    {
        SqlConnectionStringBuilder builder;
        SqlConnection connection;

        public SqlHelper()
        {
            this.builder = new SqlConnectionStringBuilder();
            this.builder.DataSource = "PC7386";
            this.builder.InitialCatalog = "RPGCharacter";
            this.builder.IntegratedSecurity = true;

            connection = new SqlConnection(builder.ConnectionString);
        }

        public void InsertCharacter(string name, GenderEnum gender, RaceEnum race, SubclassEnum className) {
            using (connection)
            {
                connection.Open();
                string sql = "INSERT INTO Characters(Name, Gender, Race, ClassId) VALUES(@param1,@param2,@param3,@param4)";
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.AddWithValue("@param1", name);
                    cmd.Parameters.AddWithValue("@param2", gender);
                    cmd.Parameters.AddWithValue("@param3", race);
                    cmd.Parameters.AddWithValue("@param4", className);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
