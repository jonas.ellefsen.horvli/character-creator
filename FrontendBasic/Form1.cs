﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
// using Logic;
using RPGCharacterGeneratorLogic;

namespace FrontendBasic
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cmbClass.DataSource = Enum.GetValues(typeof(SubclassEnum));
            cmbRace.DataSource = Enum.GetValues(typeof(RaceEnum));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        private void btnCreateCharacter(object sender, EventArgs e)
        {
            string name = txtName.Text;
            GenderEnum gender;
            SubclassEnum className = (SubclassEnum)Enum.Parse(typeof(SubclassEnum), cmbClass.SelectedItem.ToString());
            RaceEnum race = (RaceEnum)Enum.Parse(typeof(RaceEnum), cmbRace.SelectedItem.ToString());
            dynamic character;
            if (rbMale.Checked)
            {
                gender = GenderEnum.Male;
            } else
            {
                gender = GenderEnum.Female;
            }
            if (name.Equals(""))
            {
                MessageBox.Show("Please enter a name.");
                return;
            }

            switch (className)
            {
                case SubclassEnum.Knight:
                    character = new Knight(name, race, gender);
                    break;
                case SubclassEnum.Battlemage:
                    character = new Battlemage(name, race, gender);
                    break;
                case SubclassEnum.Assassin:
                    character = new Assassin(name, race, gender);
                    break;
                case SubclassEnum.Healer:
                    character = new Healer(name, race, gender);
                    break;
                case SubclassEnum.Defender:
                    character = new Defender(name, race, gender);
                    break;
                case SubclassEnum.Archer:
                    character = new Archer(name, race, gender);
                    break;
                default:
                    throw new NotImplementedException($"Class \"{className}\" is not implemented yet.");
            }
            // DataHandler.AppendCharacter(character);
            SqlHelper helper = new SqlHelper();
            helper.InsertCharacter(character.Name, character.Gender, character.Race, character.SubclassName);
            MessageBox.Show(character.ToString());
        }
    }
}
